package kpi;

import java.sql.*;

public class Model
{
    Connection dbConnection;

    public void createConnection()
    {
        try {
            Class.forName("org.postgresql.Driver");
            String URL = "jdbc:postgresql://localhost:5432/blog";
            dbConnection = DriverManager.getConnection(URL,"postgres","12345");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void insertComment(String text, int id_user, int id_post)
    {
        try {
            String sqlInsert = "INSERT INTO public.comments(" +
                    "text, date, id_user, id_post) " +
                    "VALUES (?, ?, ?, ?);";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1, text);
            prpInsertStmt.setDate(2, new Date(System.currentTimeMillis()));
            prpInsertStmt.setInt(3, id_user);
            prpInsertStmt.setInt(4, id_post);
            prpInsertStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }

    }

    public void insertPost(String title, String text, int id_user, boolean is_acrive) {
        try {
            String sqlInsert = "INSERT INTO public.posts(" +
                    "name, text, date, id_user, is_active) " +
                    "VALUES (?, ?, ?, ?, ?);";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1,title);
            prpInsertStmt.setString(2,text);
            prpInsertStmt.setDate(3, new Date(System.currentTimeMillis()));
            prpInsertStmt.setInt(4, id_user);
            prpInsertStmt.setBoolean(5, is_acrive);
            prpInsertStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void insertUser(String login, String firstName, String lastName)
    {
        try {
            String sqlInsert = "INSERT INTO public.users(" +
                    "login, firstname, lastname) " +
                    "VALUES (?, ?, ?);";
            PreparedStatement prpInsertStmt = dbConnection.prepareStatement(sqlInsert);
            prpInsertStmt.setString(1,login);
            prpInsertStmt.setString(2,firstName);
            prpInsertStmt.setString(3,lastName);
            prpInsertStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public ResultSet selectAllComments()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM comments";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet selectAllPosts()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM posts";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet selecAllUsers()
    {
        ResultSet rs = null;
        try {
            String sqlSelectAll = "SELECT * FROM users";
            Statement statement = dbConnection.createStatement();
            rs = statement.executeQuery(sqlSelectAll);
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet selectCommentsAndPostsInDiapason(Date firstDate, Date lastDate){
        ResultSet rs = null;
        try {
            String select = "SELECT * FROM comments, posts " +
                    "WHERE (posts.date >= ? AND posts.date <= ?) " +
                    "and (comments.date >= ? AND comments.date <= ?)";
            PreparedStatement prpSelectStmt = dbConnection.prepareStatement(select);
            prpSelectStmt.setDate(1,firstDate);
            prpSelectStmt.setDate(2,lastDate);
            prpSelectStmt.setDate(3,firstDate);
            prpSelectStmt.setDate(4,lastDate);
            rs = prpSelectStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public ResultSet selectPostsWhereActiveIs(boolean is_active){
        ResultSet rs = null;
        try{
            String select = "SELECT * FROM posts WHERE is_active = ?";
            PreparedStatement prpSelectStmt = dbConnection.prepareStatement(select);
            prpSelectStmt.setBoolean(1, is_active);
            rs = prpSelectStmt.executeQuery();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
        return rs;
    }

    public void updateComment(int id, String newText){
        try {
            String sqlUpdate = "UPDATE public.comments " +
                    "SET text=? " +
                    "WHERE id=?";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,newText);
            prpUpdateStmt.setInt(2,id);
            prpUpdateStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void updatePost(String name, String newName, String newText, boolean is_active){
        try {
            String sqlUpdate = "UPDATE public.posts " +
                    "SET name=?, text=?, is_active=? " +
                    "WHERE name=?;";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,newName);
            prpUpdateStmt.setString(2,newText);
            prpUpdateStmt.setBoolean(3,is_active);
            prpUpdateStmt.setString(4,name);
            prpUpdateStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void updateUser(String login, String newLogin, String newFirstName, String newLastName){
        try {
            String sqlUpdate = "UPDATE public.users " +
                    "SET login=?, firstname=?, lastname=? " +
                    "WHERE login=?;";
            PreparedStatement prpUpdateStmt = dbConnection.prepareStatement(sqlUpdate);
            prpUpdateStmt.setString(1,newLogin);
            prpUpdateStmt.setString(2,newFirstName);
            prpUpdateStmt.setString(3,newLastName);
            prpUpdateStmt.setString(4,login);
            prpUpdateStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void deleteComment(int id){
        try {
            String sqlDelete = "DELETE FROM public.comments WHERE id=?;";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setInt(1,id);
            prpDeleteStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void deletePostst(int id){
        try {
            String sqlDelete = "DELETE FROM public.posts WHERE id=?;";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setInt(1,id);
            prpDeleteStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void deleteUser(int id){
        try {
            String sqlDelete = "DELETE FROM public.users WHERE id=?;";
            PreparedStatement prpDeleteStmt = dbConnection.prepareStatement(sqlDelete);
            prpDeleteStmt.setInt(1, id);
            prpDeleteStmt.executeUpdate();
        } catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public ResultSet wordSearch(String word) {
        ResultSet rs = null;
        try{
            String sqlWordSearch = "SELECT id , ts_headline(text,q, 'StartSel=<!>, StopSel=<!>') "+
                    "FROM comments  , to_tsquery(?) as q " +
                    "WHERE NOT to_tsvector(text) @@ q";
            PreparedStatement prpSearchStmt = dbConnection.prepareStatement(sqlWordSearch);
            prpSearchStmt.setString(1 , word);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return  rs;
    }

    public ResultSet phraseSearch(String phrase) {
        ResultSet rs = null;
        try{
            String sqlWordSearch = "SELECT id , ts_headline(text ,q, 'StartSel=<!>, StopSel=<!>') "+
                    "FROM comments  , phraseto_tsquery(?) as q " +
                    "WHERE to_tsvector(text) @@ q";
            PreparedStatement prpSearchStmt = dbConnection.prepareStatement(sqlWordSearch);
            prpSearchStmt.setString(1 , phrase);
            rs = prpSearchStmt.executeQuery();
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return  rs;
    }
}