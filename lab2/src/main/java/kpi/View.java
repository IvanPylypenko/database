package kpi;

import java.sql.ResultSet;
import java.sql.SQLException;

public class View {
    public void show_from_table_Comment(ResultSet rs, boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println("TABLE Comments");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getInt(5));
                System.out.println("text: " + rs.getString(1));
                System.out.println("date: " + rs.getDate(2));
                System.out.println("id_user: " + rs.getInt(3));
                System.out.println("id_post: " + rs.getInt(4));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_Post(ResultSet rs , boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println("TABLE Post");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getInt(5));
                System.out.println("name: " + rs.getString(1));
                System.out.println("text: " + rs.getString(2));
                System.out.println("date: " + rs.getDate(3));
                System.out.println("id_user: " + rs.getInt(4));
                System.out.println("is_acteve: " + rs.getBoolean(6));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void show_from_table_User(ResultSet rs, boolean showTitle)
    {
        if(showTitle) {
            System.out.println("-------------------------------------");
            System.out.println("TABLE User");
        }
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getInt(4));
                System.out.println("login: " + rs.getString(1));
                System.out.println("firstName: " + rs.getString(2) );
                System.out.println("lastName: " + rs.getString(3));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }


    public void showMainMenu() {
        System.out.println("Main Menu");
        System.out.println("Choose operation");
        System.out.println("1) Table Comments");
        System.out.println("2) Table Posts");
        System.out.println("3) Table Users");
        System.out.println("4) Search in date range ");
        System.out.println("5) Search word");
        System.out.println("6) Random gen");
        System.out.println("7) Exit");
        System.out.print("-> ");
    }

    public  void showMenuTableOperations(String tableName){
        System.out.println("-+-+-+-+- Table " + tableName + " Menu -+-+-+-+-" );
        System.out.println("1) Show");
        System.out.println("2) Insert");
        System.out.println("3) Update");
        System.out.println("4) Delete");
        System.out.println("5) To Main Menu");

        System.out.print("-> ");
    }

    public  void showSearchInRangeDate(ResultSet rs){
        try{
            while(rs.next()){
                System.out.println("-----------------------------------");
                System.out.println("text: " + rs.getString(1));
                System.out.println("data: " + rs.getDate(2));
                System.out.println("id_user: " + rs.getInt(3));
                System.out.println("id_post: " + rs.getInt(4));
                System.out.println("id (comment): " + rs.getInt(5));
                System.out.println("name: " + rs.getString(6));
                System.out.println("text: " + rs.getString(7));
                System.out.println("date: " + rs.getDate(8));
                System.out.println("id_user: " + rs.getInt(9) );
                System.out.println("id(post): " + rs.getInt(10));
                System.out.println("is_active: " + rs.getBoolean(11));
            }
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }

    public void showInRangeDateMenu(){
        System.out.println("Search In Range Date");
        System.out.println("1) Static search 1");
        System.out.println("2) Static search 2");
        System.out.println("3) Dynamic search");
        System.out.println("4) To main menu");
        System.out.print("-> ");
    }

    public void showFullTextSearchMenu() {
        System.out.println("1) Word search from Comments texts");
        System.out.println("2) Phrase search from Comments texts");
        System.out.println("3) To main menu");
        System.out.print("-> ");
    }

    public void showTextSearchResult(ResultSet rs) {
        try {
            while (rs.next()) {
                System.out.println("-----------------------------------");
                System.out.println("id: " + rs.getString(1));
                System.out.println("text: " + rs.getString(2));
            }
            System.out.println("-----------------------------------");
        }catch (SQLException sql_except){
            sql_except.printStackTrace();
        }
    }
}