package kpi;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Controller {
    private Model model;
    private View view;
    private Scanner scanner;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void mainMenu() throws IOException {
        int choise = -1;
        scanner = new Scanner(System.in);
        do {
            view.showMainMenu();
            try {
                choise = Integer.parseInt(scanner.nextLine());
                if(choise >= 1 && choise <=3) {
                    menuTable(choise);}
                else if (choise == 4) {
                    menuSearchInRangeDateTable();}
                else if (choise == 5){
                    fullTextSearchMenu();}
                else if (choise == 6){
                    RandomGenMenu();}
                else{
                    if(choise !=7)
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException | SQLException numberError) {
                System.out.println("Incorrect input!");
            }
        } while (choise != 7);
        scanner.close();
    }

    public void menuTable(int tableNumber)
    {
        int choise = -1;
        do {
            if(tableNumber == 1)
                view.showMenuTableOperations("Comments");
            else if(tableNumber == 2)
                view.showMenuTableOperations("Posts");
            else if(tableNumber == 3)
                view.showMenuTableOperations("Users");
            try {
                choise = Integer.parseInt(scanner.nextLine());
                switch (choise) {
                    case 1:
                        showTableMenu(tableNumber);
                        break;
                    case 2:
                        if(tableNumber == 1)
                            insertInTableCommentMenu();
                        else if(tableNumber == 2)
                            insertInTablePostMenu();
                        else if(tableNumber == 3)
                            insertInTableUserMenu();
                        break;
                    case 3:
                        if(tableNumber == 1)
                            updateInTableCommentMenu();
                        else if(tableNumber == 2)
                            updateInTablePostMenu();
                        else if(tableNumber == 3)
                            updateInTableUserMenu();
                        break;
                    case 4:
                        if(tableNumber == 1)
                            deleteFromTableCommentMenu();
                        else if(tableNumber == 2)
                            deleteFromTablePostMenu();
                        else if(tableNumber == 3)
                            deleteFromTableUserMenu();
                        break;
                    case 5:
                        break;
                    default:
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException e){
                System.out.println("Incorrect input");
            }
        } while (choise != 5);
    }

    public void showTableMenu(int tableNumber){
        ResultSet rs;
        switch (tableNumber) {
            case 1:
                rs = model.selectAllComments();
                view.show_from_table_Comment(rs , true);
                break;
            case 2:
                rs = model.selectAllPosts();
                view.show_from_table_Post(rs , true);
                break;
            case 3:
                rs = model.selecAllUsers();
                view.show_from_table_User(rs, true);
                break;
        }

        int choice = -1;
        System.out.println("1) To Table menu");
        do{
            System.out.println("-> ");
            try{
                choice = Integer.parseInt(scanner.nextLine());
                if(choice != 1)
                {
                    System.out.println("Incorrect input");
                }
            }catch (NumberFormatException e)
            {
                System.out.println("Incorrect input");
            }
        }while (choice != 1);
    }

    public void FindInTablePostMenuActive(){
        ResultSet rs;
        System.out.println("Enter is_active(0,1): ");
        int is_active = Integer.parseInt(scanner.nextLine());
        rs = model.selectPostsWhereActiveIs(is_active == 1);
        view.show_from_table_Post(rs , true);
    }

    private void insertInTableCommentMenu()
    {
        String text;
        System.out.println("Enter Comment text, id_user, id_post: ");
        text = scanner.nextLine();
        int id_user = Integer.parseInt(scanner.nextLine());
        int id_post = Integer.parseInt(scanner.nextLine());
        model.insertComment(text, id_user, id_post);
    }

    private void insertInTablePostMenu()
    {
        System.out.println("Enter Product title, text, id_user, is_active(0,1): ");
        String title = scanner.nextLine();
        String text = scanner.nextLine();
        int id_user = Integer.parseInt(scanner.nextLine());
        int is_active = Integer.parseInt(scanner.nextLine());
        model.insertPost(title, text, id_user, is_active==1);
    }

    private void insertInTableUserMenu()
    {
        System.out.println("Enter login, firstName, lastName: ");
        String login = scanner.nextLine();
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        model.insertUser(login, firstName, lastName);
    }

    private void updateInTableCommentMenu(){
        System.out.println("UpdateMenu");
        System.out.println("Enter new Comment text, then id: ");
        String newText = scanner.nextLine();
        int id = Integer.parseInt(scanner.nextLine());
        model.updateComment(id, newText);
    }

    private void updateInTablePostMenu(){
        System.out.println("UpdateMenu");
        System.out.println("Enter name, newName, newText, is_active(0,1): ");
        String name = scanner.nextLine();
        String newName = scanner.nextLine();
        String newText = scanner.nextLine();
        int is_active = Integer.parseInt(scanner.nextLine());
        model.updatePost(name, newName, newText, is_active==1);
    }

    private void updateInTableUserMenu(){
        System.out.println("UpdateMenu");
        System.out.println("Enter login, newLogin, newFirstName, newLastName: ");
        String login = scanner.nextLine();
        String newLogin = scanner.nextLine();
        String newFirstName = scanner.nextLine();
        String newLastName = scanner.nextLine();
        model.updateUser(login, newLogin, newFirstName, newLastName);
    }

    private void deleteFromTableCommentMenu(){
        System.out.println("DeleteMenu");
        System.out.println("Enter Comment id to delete: ");
        int id = Integer.parseInt(scanner.nextLine());
        model.deleteComment(id);
    }

    private void deleteFromTablePostMenu(){
        System.out.println("DeleteMenu");
        System.out.println("Enter Post id to delete: ");
        int id = Integer.parseInt(scanner.nextLine());
        model.deletePostst(id);
    }

    private void deleteFromTableUserMenu(){
        System.out.println("DeleteMenu");
        System.out.println("Enter User id to delete: ");
        int id = Integer.parseInt(scanner.nextLine());
        model.deleteUser(id);
    }



    private java.sql.Date getDate(String textToShow)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date date1;
        String dateString;
        do {
            System.out.println(textToShow);
            try {
                dateString = scanner.nextLine();
                java.util.Date date = formatter.parse(dateString);
                date1 = new java.sql.Date(date.getTime());
                break;
            }catch (ParseException e){
                System.out.println("Incorrect date format. Correct format is (dd/MM/yyyy). Try again");
            }
        }while (true);
        return  date1;
    }

    private java.sql.Date getDateFromString(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date date1 = null;
        try {
            java.util.Date dateutil = formatter.parse(date);
            date1 = new java.sql.Date(dateutil.getTime());
        }catch (ParseException e){
            System.out.println("Incorrect date format. Correct format is (dd/MM/yyyy).");
        }
        return date1;
    }


    private void menuSearchInRangeDateTable(){
        int choice = -1;
        do {
            view.showInRangeDateMenu();
            try {
                choice = Integer.parseInt(scanner.nextLine());
                switch (choice){
                    case 1: StaticSearch1(); break;
                    case 2: StaticSearch2(); break;
                    case 3: DynamicSearch(); break;
                    case 4: break;
                    default:
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Incorrect input!");
            }
        } while (choice != 4);

    }


    private  void StaticSearch1(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date wrDate1 = getDateFromString("01/01/2019");
        java.sql.Date wrDate2 = getDateFromString("01/01/2020");
        ResultSet rs = model.selectCommentsAndPostsInDiapason(wrDate1 , wrDate2);
        view.showSearchInRangeDate(rs);
    }

    private  void StaticSearch2(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.sql.Date wrDate1 = getDateFromString("23/09/2019");
        java.sql.Date wrDate2 = getDateFromString("01/01/2020");
        ResultSet rs = model.selectCommentsAndPostsInDiapason(wrDate1 , wrDate2);
        view.showSearchInRangeDate(rs);
    }

    private void  DynamicSearch(){
        java.sql.Date wrDate1 = getDate("Enter WrDate1 (dd/MM/yyyy): ");
        java.sql.Date wrDate2 = getDate("Enter WrDate2 (dd/MM/yyyy): ");
        ResultSet rs = model.selectCommentsAndPostsInDiapason(wrDate1 , wrDate2);
        view.showSearchInRangeDate(rs);
    }


    private void fullTextSearchMenu() {
        int choise = -1;
        do {
            view.showFullTextSearchMenu();
            try {
                choise = Integer.parseInt(scanner.nextLine());
                if(choise == 1)
                    wordSearchMenu();
                else if (choise == 2)
                    phraseSearchMenu();
                else{
                    if(choise !=3)
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Incorrect input!");
            }
        } while (choise != 3);
    }

    private void wordSearchMenu () {
        System.out.println("Enter word to search");
        String wordToSearch = scanner.nextLine();
        ResultSet rs = model.wordSearch(wordToSearch);
        System.out.println("Word Search Result");
        try {
            if (!rs.isBeforeFirst()){
                System.out.println("Nothing was found!");
            } else {
                view.showTextSearchResult(rs);
            }
        }catch (SQLException sqlExcept){
            sqlExcept.printStackTrace();
        }
    }

    private void phraseSearchMenu () {
        System.out.println("Enter phrase to search");
        String phraseToSearch = scanner.nextLine();
        ResultSet rs = model.phraseSearch(phraseToSearch);
        System.out.println("Phrase Search Result");
        try {
            if (!rs.isBeforeFirst()){
                System.out.println("Nothing was found!");
            } else {
                view.showTextSearchResult(rs);
            }
        }catch (SQLException sqlExcept){
            sqlExcept.printStackTrace();
        }
    }

    private void RandomGenMenu() throws SQLException {
        System.out.println("Enter count: ");
        int count = Integer.parseInt(scanner.nextLine());
        ArrayList<Integer> userIds = new ArrayList<>();
        ArrayList<Integer> postIds = new ArrayList<>();
        ResultSet rs;
        rs = model.selecAllUsers();
        while (rs.next()) {
            userIds.add(rs.getInt(4));
        }
        rs = model.selectAllPosts();
        while (rs.next()) {
            postIds.add(rs.getInt(5));
        }
        if(count > 0) {
            for(int i = 0; i < count; i++){
                Random clientText = new Random();
                model.insertComment(clientText.toString(),userIds.get((int) (Math.random() * userIds.size())),
                                                            postIds.get((int) (Math.random() * postIds.size())));
            }
        }
    }
}
