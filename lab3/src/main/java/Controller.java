
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Controller {
    private Model model;
    private View view;
    private Scanner scanner;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void mainMenu() throws IOException {
        int choise = -1;
        scanner = new Scanner(System.in);
        do {
            view.showMainMenu();
            try {
                choise = Integer.parseInt(scanner.nextLine());
                if(choise >= 1 && choise <=3) {
                    menuTable(choise);}
                else{
                    if(choise !=4)
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException numberError) {
                System.out.println("Incorrect input!");
            }
        } while (choise != 4);
        scanner.close();
    }

    public void menuTable(int tableNumber)
    {
        int choise = -1;
        do {
            if(tableNumber == 1)
                view.showMenuTableOperations("Comments");
            else if(tableNumber == 2)
                view.showMenuTableOperations("Posts");
            else if(tableNumber == 3)
                view.showMenuTableOperations("Users");
            try {
                choise = Integer.parseInt(scanner.nextLine());
                switch (choise) {
                    case 1:
                        showTableMenu(tableNumber);
                        break;
                    case 2:
                        if(tableNumber == 1)
                            insertInTableCommentMenu();
                        else if(tableNumber == 2)
                            insertInTablePostMenu();
                        else if(tableNumber == 3)
                            insertInTableUserMenu();
                        break;
                    case 3:
                        if(tableNumber == 1)
                            updateInTableCommentMenu();
                        else if(tableNumber == 2)
                            updateInTablePostMenu();
                        else if(tableNumber == 3)
                            updateInTableUserMenu();
                        break;
                    case 4:
                        if(tableNumber == 1)
                            deleteFromTableCommentMenu();
                        else if(tableNumber == 2)
                            deleteFromTablePostMenu();
                        else if(tableNumber == 3)
                            deleteFromTableUserMenu();
                        break;
                    case 5:
                        break;
                    default:
                        System.out.println("Incorrect input");
                }
            } catch (NumberFormatException e){
                System.out.println("Incorrect input");
            }
        } while (choise != 5);
    }

    public void showTableMenu(int tableNumber){
        switch (tableNumber) {
            case 1:
                model.selectAllComments();
                break;
            case 2:
                model.selectAllPosts();

                break;
            case 3:
                model.selecAllUsers();
                break;
        }

        int choice = -1;
        System.out.println("1) To Table menu");
        do{
            System.out.println("-> ");
            try{
                choice = Integer.parseInt(scanner.nextLine());
                if(choice != 1)
                {
                    System.out.println("Incorrect input");
                }
            }catch (NumberFormatException e)
            {
                System.out.println("Incorrect input");
            }
        }while (choice != 1);
    }

    private void insertInTableCommentMenu()
    {
        String text;
        System.out.println("Enter Comment text, id_user, id_post: ");
        text = scanner.nextLine();
        int id_user = Integer.parseInt(scanner.nextLine());
        int id_post = Integer.parseInt(scanner.nextLine());
        model.insertComment(text, id_user, id_post);
    }

    private void insertInTablePostMenu()
    {
        System.out.println("Enter Product title, text, id_user: ");
        String title = scanner.nextLine();
        String text = scanner.nextLine();
        int id_user = Integer.parseInt(scanner.nextLine());
        model.insertPost(title, text, id_user);
    }

    private void insertInTableUserMenu()
    {
        System.out.println("Enter login, firstName, lastName: ");
        String login = scanner.nextLine();
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        model.insertUser(login, firstName, lastName);
    }

    private void updateInTableCommentMenu(){
        System.out.println("UpdateMenu");
        System.out.println("Enter new Comment text, then id: ");
        String newText = scanner.nextLine();
        int id = Integer.parseInt(scanner.nextLine());
        model.updateComment(id, newText);
    }

    private void updateInTablePostMenu(){
        System.out.println("UpdateMenu");
        System.out.println("Enter name, newName, newText: ");
        String name = scanner.nextLine();
        String newName = scanner.nextLine();
        String newText = scanner.nextLine();
        model.updatePost(name, newName, newText);
    }

    private void updateInTableUserMenu(){
        System.out.println("UpdateMenu");
        System.out.println("Enter login, newLogin, newFirstName, newLastName: ");
        String login = scanner.nextLine();
        String newLogin = scanner.nextLine();
        String newFirstName = scanner.nextLine();
        String newLastName = scanner.nextLine();
        model.updateUser(login, newLogin, newFirstName, newLastName);
    }

    private void deleteFromTableCommentMenu(){
        System.out.println("DeleteMenu");
        System.out.println("Enter Comment id to delete: ");
        int id = Integer.parseInt(scanner.nextLine());
        model.deleteComment(id);
    }

    private void deleteFromTablePostMenu(){
        System.out.println("DeleteMenu");
        System.out.println("Enter Post id to delete: ");
        int id = Integer.parseInt(scanner.nextLine());
        model.deletePosts(id);
    }

    private void deleteFromTableUserMenu(){
        System.out.println("DeleteMenu");
        System.out.println("Enter User id to delete: ");
        int id = Integer.parseInt(scanner.nextLine());
        model.deleteUser(id);
    }


}
