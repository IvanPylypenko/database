import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.List;

@Entity
@TableGenerator(name = "Users")
public class Users {
    private Integer id;
    public String login;
    public String firstName;
    public String lastName;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getLogin(){
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void getAll(){
        List<Users> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Users");
            employerList = (List<Users>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Users emp : employerList) {
                System.out.println("<------->");
                System.out.println("ID: " + emp.id);
                System.out.println("LOGIN " + emp.login);
                System.out.println("FirstName " + emp.firstName);
                System.out.println("LastName " + emp.lastName);
            }
        }
    }

    public void Insert(String login, String fName, String lName) {

        setLogin(login);
        setLastName(lName);
        setFirstName(fName);

    }

    public void delete(int id) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Users WHERE id = :id");
            query.setParameter("id", id);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(String login, String newLogin, String fName, String lName) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Users set login = :newlog, firstname = :fname, lastname = :lName where login = :log");
            query.setParameter("newlog", newLogin);
            query.setParameter("fname", fName);
            query.setParameter("lName", lName);
            query.setParameter("log", login);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
