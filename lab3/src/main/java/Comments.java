import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.List;

@Entity
@TableGenerator(name = "Comments")
public class Comments {
    private Integer id;
    public String text;
    public Integer id_user;
    public Integer id_post;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public Integer getId_post() {
        return id_post;
    }

    public void setId_post(Integer id_post) {
        this.id_post = id_post;
    }

    public void getAll(){
        List<Comments> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Comments");
            employerList = (List<Comments>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Comments emp : employerList) {
                System.out.println("<------->");
                System.out.println("ID: " + emp.id);
                System.out.println("Text " + emp.text);
                System.out.println("ID_USER " + emp.id_user);
                System.out.println("ID_POST " + emp.id_post);
            }
        }
    }

    public void Insert(String text, int id_user, int id_post) {
        setText(text);
        setId_user(id_user);
        setId_post(id_post);
    }

    public void delete(int id) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Comments WHERE id=:id");
            query.setParameter("id", id);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(int id, String text) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Comments set text = :text where id = :id");
            query.setParameter("text", text);
            query.setParameter("id", id);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
