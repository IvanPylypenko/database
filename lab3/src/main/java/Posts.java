import org.hibernate.Query;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.List;

@Entity
@TableGenerator(name = "Posts")
public class Posts {
    private Integer id;
    public String name;
    public String text;
    public Integer id_user;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer ctId) {
        this.id = ctId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public void getAll(){
        List<Posts> employerList = null;
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("From Posts");
            employerList = (List<Posts>) query.list();
        }
        if(employerList != null && !employerList.isEmpty()) {
            for(Posts emp : employerList) {
                System.out.println("<------->");
                System.out.println("ID: " + emp.id);
                System.out.println("Name " + emp.name);
                System.out.println("Text " + emp.text);
                System.out.println("ID_USER " + emp.id_user);
            }
        }
    }

    public void Insert(String name, String text, int id_user) {
        setName(name);
        setText(text);
        setId_user(id_user);
    }

    public void delete(int id) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("DELETE from Posts WHERE id = :id");
            query.setParameter("id", id);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
    public void update(String name, String text, String newName) {
        try(Session session = HibernateUtil.getSession()) {
            session.beginTransaction();
            Query query = session.createQuery("update Posts set name = :newName, text = :text where name = :name");
            query.setParameter("newName", newName);
            query.setParameter("text", text);
            query.setParameter("name", name);
            session.getTransaction().commit();
            int result = query.executeUpdate();
        }
    }
}
