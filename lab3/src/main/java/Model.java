import org.hibernate.Session;

import java.sql.*;

public class Model
{

    public void insertComment(String text, int id_user, int id_post)
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Comments com = new Comments();
            com.Insert(text, id_user, id_post);
            session.save(com);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }

    }

    public void insertPost(String title, String text, int id_user) {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Posts com = new Posts();
            com.Insert(title, text, id_user);
            session.save(com);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void insertUser(String login, String firstName, String lastName)
    {
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Users com = new Users();
            com.Insert(login, firstName, lastName);
            session.save(com);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void selectAllComments()
    {
        try(Session session = HibernateUtil.getSession()){
            Comments com = new Comments();
            com.getAll();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void selectAllPosts()
    {
        try(Session session = HibernateUtil.getSession()){
            Posts com = new Posts();
            com.getAll();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void selecAllUsers()
    {
        try(Session session = HibernateUtil.getSession()){
            Users com = new Users();
            com.getAll();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }


    public void updateComment(int id, String newText){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Comments com = new Comments();
            com.update(id, newText);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void updatePost(String name, String newName, String newText){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Posts com = new Posts();
            com.update(name,newText, newName);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void updateUser(String login, String newLogin, String newFirstName, String newLastName){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Users com = new Users();
            com.update(login, newLogin, newFirstName, newLastName);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void deleteComment(int id){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Comments com = new Comments();
            com.delete(id);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void deletePosts(int id){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Posts com = new Posts();
            com.delete(id);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }

    public void deleteUser(int id){
        try(Session session = HibernateUtil.getSession()){
            session.beginTransaction();
            Users com = new Users();
            com.delete(id);
            session.getTransaction().commit();
        }catch (Throwable cause) {
            cause.printStackTrace();
        }
    }
}